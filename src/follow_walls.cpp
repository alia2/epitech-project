
#include "epitech-project/follow_walls.hpp"

using namespace std::chrono_literals;

FollowWalls::FollowWalls()
: Node("follow_walls_node")
{
  /************************************************************
  ** Initialise variables
  ************************************************************/
    state=1;
    Detected_Mark=false;
    scan_front=0.0; 
    scan_right=0.0;
    scan_data_[0]=0.0;
    scan_data_[1]=0.0;

  /************************************************************
  ** Initialise ROS publishers and subscribers
  ************************************************************/
  auto qos = rclcpp::QoS(rclcpp::KeepLast(10));

  // Initialise publishers
  cmd_vel_pub_ = this->create_publisher<geometry_msgs::msg::Twist>("cmd_vel", qos);

  // Initialise subscribers
  scan_sub_ = this->create_subscription<sensor_msgs::msg::LaserScan>(
    "scan", rclcpp::SensorDataQoS(), std::bind(&FollowWalls::scan_callback, this, std::placeholders::_1));
  
  /************************************************************
  ** Initialise ROS timers
  ************************************************************/
  
  update_timer_ = this->create_wall_timer(10ms, std::bind(&FollowWalls::demo, this));

  RCLCPP_INFO(this->get_logger(), "Turtlebot3 simulation node has been initialised");
}

FollowWalls::~FollowWalls()
{
  RCLCPP_INFO(this->get_logger(), "Turtlebot3 simulation node has been terminated");
}

/********************************************************************************
** Callback functions for ROS subscribers
********************************************************************************/
void FollowWalls::scan_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg)
{
  size= msg->ranges.size();
  uint16_t scan_angle[2] = {0,330};

      for (int num = 0; num < 2; num++)
      {
       if (std::isinf(msg->ranges.at(scan_angle[num])))
        {
         scan_data_[num] = msg->range_max;
        }
       else
        {
         scan_data_[num] = msg->ranges.at(scan_angle[num]);
        }
      }
      scan_right=scan_data_[1];
      scan_front=scan_data_[0];  
      //RCLCPP_INFO(this->get_logger(), "scan Front%f", scan_front );
      //RCLCPP_INFO(this->get_logger(), "scan right %f",scan_right);
}

void FollowWalls::Move_the_robot()
 {
  auto mf_speed =geometry_msgs::msg::Twist();

      switch(state) {
       case 1 :       //move_forward;
        mf_speed.linear.x=0.5;
        mf_speed.angular.z=0.0;
        break;
       case 2 :       //turn_left;
        mf_speed.linear.x=0.0;
        mf_speed.angular.z=0.5;
        break;
       case 3 :       //curve_right;
        mf_speed.linear.x=0.2;
        mf_speed.angular.z=-0.2;      
        break;
       case 4 :       //stop_moving;
        mf_speed.linear.x=0.0;
        mf_speed.angular.z=0.0;
        break;
       default : 
        break;
      }

      cmd_vel_pub_->publish(mf_speed);
 }

/********************************************************************************
** Update functions
********************************************************************************/
 void FollowWalls::update_state()
 {
      double dFront=1.5;
      double dRight=1.5;
      where_Am_I(dFront,dRight);
      if (state==1)
       {
         if (FrontWall) 
          state=2;
         else if(!RightWall)
          state=3;
       }
      else if(state==2)
       {
        if(RightWall && !FrontWall)
         state=1;
       }
      else if(state==3)
      {
        if (RightWall)
         state=1;
      }

  }


  void FollowWalls::where_Am_I(double dFront, double dRight)
  {
        if (scan_front<dFront)
          FrontWall=true;
        else
          FrontWall=false;
        if (scan_right<dRight)
          RightWall=true;
        else
          RightWall=false;
    
  }
    
  void FollowWalls::demo()
  {    
        // rclcpp::sleep_for(std::chrono::seconds(10));
       // RCLCPP_INFO(this->get_logger(), "state %i", state);
       // RCLCPP_INFO(this->get_logger(), "%i", FrontWall);
       // RCLCPP_INFO(this->get_logger(), "%i",RightWall); 
        Move_the_robot();
        update_state();
        
  }

/*******************************************************************************
** Main
*******************************************************************************/
int main(int argc, char** argv)
{ 
  rclcpp::sleep_for(std::chrono::seconds(10));
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<FollowWalls>());
  rclcpp::shutdown();

  return 0;
}
