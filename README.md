Get the simulation environment :

```shell
$ cd ~
$ mkdir adehome-epitech-project
$ cd adehome-epitech-project
$ touch .adehome
$ git clone https://gitlab.com/deb0ch/ade-turtlebot3.git
$ cd ~/adehome-epitech-project/ade-turtlebot3
$ ade start
$ ade enter
```

Get the project :

```shell
$ cd turtlebot3_ws/src
$ git clone https://gitlab.com/alia2/epitech-project
```

Build the project :

```shell
$ cd ~/turtlebot3_ws
$ colcon build
$ source install/setup.{z}sh
```


Run the project :

1 - To run the wall follower :
```shell
$ ros2 launch epitech-project follow_walls.launch.py
```

2 - To launch turtlebot3 with gazebo and the maze world :
```shell
$ ros2 launch epitech-project maze_world.launch.py
```

3 - To map the environment while navigating manually the turtlebot3, run:
```shell
$ ros2 launch epitech-project mapping.launch.py
```
In another shell, run:
```shell
$ cd ~/adehome-epitech-project/ade-turtlebot3
$ ade enter
$ ros2 run turtlebot3_teleop teleop_keyboard
```
To save the generated map,run in a new terminal:
```shell
$ cd ~/adehome_epitech_project/ade_turtlebot3_project
$ ade enter
$ ros2 run nav2_map_server map_saver_cli
```

4 - To navigate autonomously in a prebuilt map, run :
```shell
$ ros2 launch epitech-project navigation.launch.py
```
Use the 2D position estimate and the navigation tools to select a pose destination and let the robot plan its way and navigate 
towards the desired destination. 

5 - To navigate autonomously while mapping the environment using SLAM, run :
```shell
$ ros2 launch epitech-project navigation_mapping.launch.py
```
Make sure to select a destination pose within the field of view of the robot.