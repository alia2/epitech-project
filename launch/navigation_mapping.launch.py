import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument, ExecuteProcess
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


TURTLEBOT3_MODEL = os.environ['TURTLEBOT3_MODEL']


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='true')
    launch_file_dir = os.path.join(get_package_share_directory('epitech-project'), 'launch')
    bringup_launch_file_dir= os.path.join(get_package_share_directory('nav2_bringup'), 'launch')
    map_dir = LaunchConfiguration('map', default=os.path.join(get_package_share_directory('epitech-project'),'maps','map.yaml'))
    param_dir = LaunchConfiguration('params', default=os.path.join(get_package_share_directory('turtlebot3_navigation2'),'param', TURTLEBOT3_MODEL + '.yaml'))
    rviz_config_file=os.path.join(get_package_share_directory('nav2_bringup'),'rviz','nav2_default_view.rviz')
    slam='True'
       
    
                       
    

    return LaunchDescription([


        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([bringup_launch_file_dir, '/bringup_launch.py']),
            launch_arguments={
                'map': map_dir,
                'use_sim_time': use_sim_time,
                'params': param_dir,
                'slam': slam}.items(),
        ),

        Node(package='tf2_ros',
                     node_executable='static_transform_publisher',
                     node_name='static_transform_publisher',
                     output='log',
                     arguments=['0.0', '0.0', '0.0', '0.0', '0.0', '0.0', 'world', 'odom']),

        Node(package='tf2_ros',
                     node_executable='static_transform_publisher',
                     node_name='static_transform_publisher',
                     output='log',
                     arguments=['0.0', '0.0', '0.0', '0.0', '0.0', '0.0', 'world', 'map']),


        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([launch_file_dir, '/maze_world.launch.py']),
            launch_arguments={'use_sim_time': use_sim_time}.items(),
        ),


        Node(
            package='rviz2',
            node_executable='rviz2',
            node_name='rviz2',
            arguments=['-d', rviz_config_file],
            parameters=[{'use_sim_time': use_sim_time}],
            output='screen'),



    ])





