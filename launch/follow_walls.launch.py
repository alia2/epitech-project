import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument, ExecuteProcess
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import Node


TURTLEBOT3_MODEL = os.environ['TURTLEBOT3_MODEL']


def generate_launch_description():
    use_sim_time = LaunchConfiguration('use_sim_time', default='true')
    launch_file_dir = os.path.join(get_package_share_directory('epitech-project'), 'launch')
    
    follow_walls_node=Node(package='epitech-project',
                            executable='follow_walls',
                            name='follow_walls_node',
                            output='screen')


    return LaunchDescription([
        
        follow_walls_node,
        
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource([launch_file_dir, '/maze_world.launch.py']),
            launch_arguments={'use_sim_time': use_sim_time,
                              'x': '0.5',
                              'y':'1.5',
                              'z':'0.0',
                              'R':'0.0',
                              'P':'0.0',
                              'Y':'3.14'}.items(),
        )
    ])
