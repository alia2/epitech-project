#ifndef FOLLOW_WALLS_HPP_
#define FOLLOW_WALLS_HPP_
#include <geometry_msgs/msg/twist.hpp>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/laser_scan.hpp>
#include <math.h> 
#include <memory>
#include <array>


class FollowWalls : public rclcpp::Node
{
 public:
   FollowWalls();
  ~FollowWalls();

 private:
  // ROS topic publishers
  rclcpp::Publisher<geometry_msgs::msg::Twist>::SharedPtr cmd_vel_pub_;

  // ROS topic subscribers
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr scan_sub_;
 
  // Variables
    int state;
    bool FrontWall, RightWall, Detected_Mark;
    double scan_front, scan_right;
    double size, scan_data_[2];

  // ROS timer
  rclcpp::TimerBase::SharedPtr update_timer_;

  // Function prototypes
  void update_state();
  void where_Am_I(double dFront, double dRight);
  void Move_the_robot();
  void demo();
  void scan_callback(const sensor_msgs::msg::LaserScan::SharedPtr msg);

};
#endif 
